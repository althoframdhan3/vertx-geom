-- ----------------------------
-- Table structure for geometry
-- ----------------------------
DROP TABLE IF EXISTS `geometry`;
CREATE TABLE `geometry`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL UNIQUE,
  `description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `formula_area` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `formula_perimeter` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created_time` datetime(0) NULL DEFAULT NULL,
  `last_modified_time` datetime(0) NULL DEFAULT NULL,
  `deleted_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

INSERT INTO `geometry`
(name,description,formula_area,formula_perimeter,is_deleted,created_time,last_modified_time,deleted_time) VALUES
('square','Geometry with all side equal in length and each side perpendicular to each others ','s^2','4*s',0,NULL,'2020-08-23 11:23:31.0',NULL)
;