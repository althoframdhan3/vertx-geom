package id.lmnzr.example.geom.database.query;

import io.vertx.core.json.JsonArray;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Insert query generator.
 */
public class InsertQueryGenerator {
    private final String tableName;
    private final List<String> insertField;
    private final List<String> insertValuePlaceholder;
    private final JsonArray queryParameter;

    /**
     * Instantiates a new Insert query generator.
     *
     * @param tableName the table name
     */
    public InsertQueryGenerator(String tableName) {
        this.tableName = tableName;
        this.insertField = new ArrayList<>();
        this.insertValuePlaceholder = new ArrayList<>();
        this.queryParameter = new JsonArray();
    }

    /**
     * Gets query parameter.
     *
     * @return the query parameter
     */
    public JsonArray getQueryParameter() {
        return queryParameter;
    }

    /**
     * Add field value insert query generator.
     *
     * @param fieldName the field name
     * @param value     the value
     */
    public void addFieldValue(String fieldName, Object value) {
        insertField.add(fieldName);
        insertValuePlaceholder.add("?");
        queryParameter.add(value);
    }

    /**
     * Generate string.
     *
     * @return the string
     */
    public String generate() {
        return String.format("INSERT INTO %s (", tableName) +
            String.join(", ", insertField) +
            ") VALUES (" +
            String.join(", ", insertValuePlaceholder) +
            ")";
    }
}
