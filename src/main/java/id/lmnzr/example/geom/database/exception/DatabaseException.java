package id.lmnzr.example.geom.database.exception;

/**
 * The type Database exception.
 */
public class DatabaseException extends Exception{
    /**
     * Instantiates a new Database exception.
     *
     * @param message the message
     */
    public DatabaseException(String message) {
        super(message);
    }
}
