package id.lmnzr.example.geom.database.driver;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.URI;

/**
 * MariaDB JDBC Client Driver
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MariaDB {
    /**
     * Create non shared jdbc client.
     *
     * @param vertx       the vertx object
     * @param databaseUri the database databaseUri
     * @return the non shared JDBC Client for mariadb
     */
    static JDBCClient createNonShared(Vertx vertx, URI databaseUri) {
        return JDBCClient.createNonShared(vertx, readConfig(databaseUri));
    }

    /**
     * Create shared jdbc client. The client data source name will be the same
     * as the database name
     *
     * @param vertx       the vertx object
     * @param databaseUri the database databaseUri
     * @return the shared JDBC Client for mariadb
     */
    static JDBCClient createShared(Vertx vertx, URI databaseUri) {
        JsonObject config = readConfig(databaseUri);
        return JDBCClient.createShared(vertx, config, config.getString("url"));
    }

    /**
     * Read database configuration from URI object given and return it as json
     * object with following format :
     * <pre>
     * {
     *     "url" : connection url string,
     *     "port" : database port,
     *     "user" : username,
     *     "password" : password for given user,
     *     "database" : database name
     * }
     * </pre>
     *
     * @param databaseUri the database databaseUri
     * @return the database configuration in json object
     */
    private static JsonObject readConfig(URI databaseUri) {
        String databaseURL = "jdbc:mariadb://" + databaseUri.getHost() + databaseUri.getPath();
        return new JsonObject()
            .put("url", databaseURL)
            .put("port", databaseUri.getPort())
            .put("user", databaseUri.getUserInfo().split(":")[0])
            .put("password", databaseUri.getUserInfo().split(":").length > 1 ? databaseUri.getUserInfo().split(":")[1] : "")
            .put("database", databaseUri.getPath());
    }
}
