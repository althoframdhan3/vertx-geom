package id.lmnzr.example.geom.database.query;

import io.vertx.core.json.JsonArray;

/**
 * The type Update query generator.
 */
public class UpdateQueryGenerator extends Generator {
    private final StringBuilder setStatement;

    /**
     * Instantiates a new Update query generator.
     *
     * @param tableName the table name
     */
    public UpdateQueryGenerator(String tableName) {
        super(tableName);

        this.setStatement = new StringBuilder();
        this.queryParameter = new JsonArray();
    }

    @Override
    public JsonArray getQueryParameter() {
        return this.queryParameter;
    }

    /**
     * Generate string.
     *
     * @return the string
     */
    public String generate() {
        StringBuilder generatedQuery = new StringBuilder();

        generatedQuery.append(String.format("UPDATE %s SET ", this.tableName))
            .append(this.setStatement);

        if (!this.where.toString().equals("") && !this.where.toString().isEmpty()) {
            generatedQuery.append(" WHERE ").append(this.where.toString());
        }

        return generatedQuery.toString();
    }

    /**
     * Sets field value.
     *
     * @param fieldName  the field name
     * @param fieldValue the field value
     */
    public void setFieldValue(String fieldName, Object fieldValue) {
        this.setStatement.append(this.setStatement.length() > 0 ? ", " : "")
            .append(String.format("%s = ?", fieldName));
        this.queryParameter.add(fieldValue);

    }
}

