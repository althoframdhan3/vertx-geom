package id.lmnzr.example.geom.verticles.geometry.service.eventbus;

import id.lmnzr.example.geom.utils.PojoMapper;
import id.lmnzr.example.geom.verticles.geometry.GeometryConfig;
import id.lmnzr.example.geom.verticles.geometry.model.Geometry;
import id.lmnzr.example.geom.verticles.geometry.service.GeometryService;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;


/**
 * The type Geometry eventbus.
 */
public class GeometryEventbus {

    private final Logger log = LoggerFactory.getLogger(GeometryEventbus.class);

    private final Vertx vertx;

    private final GeometryService geometryService;

    /**
     * Instantiates a new Geometry eventbus.
     *
     * @param vertx      the vertx
     * @param jdbcClient the jdbc client
     */
    public GeometryEventbus(Vertx vertx, JDBCClient jdbcClient) {
        this.vertx = vertx;
        this.geometryService = new GeometryService(jdbcClient);

        healthConsumer();
        findByIdConsumer();
        findByNameConsumer();
        fetchConsumer();
        saveConsumer();
        deleteConsumer();
    }

    private void healthConsumer() {
        this.vertx.eventBus().consumer(GeometryConfig.HEALTH, message -> {
            logConsumer(GeometryConfig.HEALTH, message.body().toString());
            geometryService.health(handler -> {
                if (handler.succeeded()) {
                    message.reply(handler.result());
                } else {
                    message.fail(500, handler.cause().getMessage());
                }
            });
        });
    }

    private void findByNameConsumer() {
        this.vertx.eventBus().consumer(GeometryConfig.FIND_BY_NAME, message -> {
            logConsumer(GeometryConfig.FIND_BY_NAME, message.body().toString());
            geometryService.findByName(message.body().toString(), handler -> {
                if (handler.succeeded()) {
                    message.reply(Json.encodePrettily(handler.result()));
                } else {
                    message.fail(500, handler.cause().getMessage());
                }
            });
        });
    }

    private void findByIdConsumer() {
        this.vertx.eventBus().consumer(GeometryConfig.FIND_BY_ID, message -> {
            logConsumer(GeometryConfig.FIND_BY_ID, message.body().toString());
            geometryService.findById(Long.valueOf(message.body().toString()), handler -> {
                if (handler.succeeded()) {
                    message.reply(Json.encodePrettily(handler.result()));
                } else {
                    message.fail(500, handler.cause().getMessage());
                }
            });
        });
    }

    private void fetchConsumer() {
        this.vertx.eventBus().consumer(GeometryConfig.FETCH, message -> {
            logConsumer(GeometryConfig.FETCH, message.body().toString());

            Integer limit = null;
            Integer offset = null;

            try {
                JsonObject jsonReq = new JsonObject(message.body().toString());
                limit = jsonReq.getInteger("limit");
                offset = jsonReq.getInteger("offset");
            } catch (Exception exception) {
                log.error(exception.getMessage());
            }

            geometryService.fetch(limit, offset, handler -> {
                if (handler.succeeded()) {
                    message.reply(Json.encodePrettily(handler.result()));
                } else {
                    message.fail(500, handler.cause().getMessage());
                }
            });
        });
    }

    private void saveConsumer() {
        this.vertx.eventBus().consumer(GeometryConfig.SAVE, message -> {
            logConsumer(GeometryConfig.SAVE, message.body().toString());
            JsonObject jsonReq = new JsonObject(message.body().toString());

            geometryService.save(PojoMapper.fromJsonObject(jsonReq, Geometry.class), handler -> {
                if (handler.succeeded()) {
                    message.reply(Json.encodePrettily(handler.result()));
                } else {
                    message.fail(500, handler.cause().getMessage());
                }
            });
        });
    }

    private void deleteConsumer() {
        this.vertx.eventBus().consumer(GeometryConfig.DELETE, message -> {
            logConsumer(GeometryConfig.DELETE, message.body().toString());
            geometryService.delete(Long.valueOf(message.body().toString()), handler -> {
                if (handler.succeeded()) {
                    message.reply(Json.encodePrettily(handler.result()));
                } else {
                    message.fail(500, handler.cause().getMessage());
                }
            });
        });
    }


    private void logConsumer(String eventbusAddress, String message) {
        log.debug(eventbusAddress + " consuming " + message);
    }
}
