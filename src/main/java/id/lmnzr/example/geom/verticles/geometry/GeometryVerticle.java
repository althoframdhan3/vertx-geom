package id.lmnzr.example.geom.verticles.geometry;

import id.lmnzr.example.geom.database.driver.ClientFactory;
import id.lmnzr.example.geom.utils.EnvUtils;
import id.lmnzr.example.geom.verticles.geometry.service.eventbus.GeometryEventbus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;

/**
 * The type Geometry verticle.
 */
public class GeometryVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(GeometryVerticle.class);

    @Override
    public void start(Promise<Void> startPromise) {
        JDBCClient jdbcClient = ClientFactory.createShared(
            vertx,
            EnvUtils.getDatabaseURIfromEnv(),
            ClientFactory.Driver.MARIADB
        );

        new GeometryEventbus(vertx, jdbcClient);

        startPromise.complete();
    }

    @Override
    public void stop() {
        log.debug("Geometry Verticle stopped");
    }
}
