package id.lmnzr.example.geom.verticles.geometry.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.lmnzr.example.geom.verticles.geometry.model.repository.GeometryRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Geometry.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "valueOf")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Geometry {
    @JsonProperty(value = GeometryRepository.ID)
    private Long id;

    @JsonProperty(value = GeometryRepository.NAME)
    private String name;

    @JsonProperty(value = GeometryRepository.DESCRIPTION)
    private String description;

    @JsonProperty(value = GeometryRepository.FORMULA_AREA)
    private String formulaArea;

    @JsonProperty(value = GeometryRepository.FORMULA_PERIMETER)
    private String formulaPerimeter;
}
