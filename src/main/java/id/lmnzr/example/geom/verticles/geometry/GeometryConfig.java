package id.lmnzr.example.geom.verticles.geometry;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * The type Geometry config.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class GeometryConfig {
    /**
     * The constant HEALTH.
     */
    public static final String HEALTH = "id.lmnzr.example.geom.verticles.geometry:health";
    /**
     * The constant FIND_BY_NAME.
     */
    public static final String FIND_BY_NAME = "id.lmnzr.example.geom.verticles.geometry:find-by-name";
    /**
     * The constant FIND_BY_ID.
     */
    public static final String FIND_BY_ID = "id.lmnzr.example.geom.verticles.geometry:find-by-id";
    /**
     * The constant DELETE.
     */
    public static final String DELETE = "id.lmnzr.example.geom.verticles.geometry:delete";
    /**
     * The constant FETCH.
     */
    public static final String FETCH = "id.lmnzr.example.geom.verticles.geometry:fetch";
    /**
     * The constant SAVE.
     */
    public static final String SAVE = "id.lmnzr.example.geom.verticles.geometry:save";
}
