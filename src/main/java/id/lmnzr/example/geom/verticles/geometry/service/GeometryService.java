package id.lmnzr.example.geom.verticles.geometry.service;

import id.lmnzr.example.geom.utils.PojoMapper;
import id.lmnzr.example.geom.verticles.geometry.model.Geometry;
import id.lmnzr.example.geom.verticles.geometry.model.repository.GeometryRepository;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

/**
 * The type Geometry service.
 */
public class GeometryService {
    private static final String SUCCESS_STATUS_FIELD = "success";
    private static final String ERROR_MESSAGE_FIELD = "error";
    private final GeometryRepository geometryRepository;

    /**
     * Instantiates a new Geometry service.
     *
     * @param jdbcClient the jdbc client
     */
    public GeometryService(JDBCClient jdbcClient) {
        this.geometryRepository = new GeometryRepository(jdbcClient);
    }

    /**
     * Health.
     *
     * @param asyncResultHandler the handler
     */
    public void health(Handler<AsyncResult<String>> asyncResultHandler) {
        asyncResultHandler.handle(Future.succeededFuture("Geometry service is working"));
    }

    /**
     * Find by id.
     *
     * @param id                 the id
     * @param asyncResultHandler the async result handler
     */
    public void findById(Long id, Handler<AsyncResult<JsonObject>> asyncResultHandler) {
        geometryRepository.findById(id, handler -> {
            if (handler.succeeded()) {
                JsonObject json = PojoMapper.toJsonObject(handler.result());
                json.put(SUCCESS_STATUS_FIELD, true);
                asyncResultHandler.handle(Future.succeededFuture(json));
            } else {
                JsonObject json = new JsonObject();
                json.put(SUCCESS_STATUS_FIELD, false);
                json.put(ERROR_MESSAGE_FIELD, handler.cause().getMessage());
                asyncResultHandler.handle(Future.succeededFuture(json));
            }
        });
    }

    /**
     * Find by name.
     *
     * @param name               the name
     * @param asyncResultHandler the async result handler
     */
    public void findByName(String name, Handler<AsyncResult<JsonObject>> asyncResultHandler) {
        geometryRepository.findByName(name, handler -> {
            if (handler.succeeded()) {
                JsonObject json = PojoMapper.toJsonObject(handler.result());
                json.put(SUCCESS_STATUS_FIELD, true);
                asyncResultHandler.handle(Future.succeededFuture(json));
            } else {
                JsonObject json = new JsonObject();
                json.put(SUCCESS_STATUS_FIELD, false);
                json.put(ERROR_MESSAGE_FIELD, handler.cause().getMessage());
                asyncResultHandler.handle(Future.succeededFuture(json));
            }
        });
    }

    /**
     * Retrieve all geometry.
     *
     * @param limit              the limit default & maximum 10
     * @param offset             the offset default 0
     * @param asyncResultHandler the async result handler
     */
    public void fetch(Integer limit, Integer offset, Handler<AsyncResult<JsonObject>> asyncResultHandler) {
        Integer retrieveLimit = null == limit || 0 > limit || 10 < limit ? 10 : limit;
        Integer retrieveOffset = null == offset || 0 > offset ? 0 : offset;

        geometryRepository.fetch(retrieveLimit, retrieveOffset, handler -> {
            if (handler.succeeded()) {
                JsonObject json = new JsonObject();
                json.put(SUCCESS_STATUS_FIELD, true);
                json.put("data", handler.result());
                asyncResultHandler.handle(Future.succeededFuture(json));
            } else {
                JsonObject json = new JsonObject();
                json.put(SUCCESS_STATUS_FIELD, false);
                json.put(ERROR_MESSAGE_FIELD, handler.cause().getMessage());
                asyncResultHandler.handle(Future.succeededFuture(json));
            }
        });
    }

    /**
     * Save.
     *
     * @param geometry           the geometry
     * @param asyncResultHandler the async result handler
     */
    public void save(Geometry geometry, Handler<AsyncResult<JsonObject>> asyncResultHandler) {
        geometryRepository.save(geometry, handler -> {
            if (handler.succeeded()) {
                JsonObject json = new JsonObject();
                json.put(SUCCESS_STATUS_FIELD, true);

                asyncResultHandler.handle(Future.succeededFuture(json));
            } else {
                JsonObject json = new JsonObject();
                json.put(SUCCESS_STATUS_FIELD, false);
                json.put(ERROR_MESSAGE_FIELD, handler.cause().getMessage());
                asyncResultHandler.handle(Future.succeededFuture(json));
            }
        });
    }

    /**
     * Delete.
     *
     * @param id                 the id
     * @param asyncResultHandler the async result handler
     */
    public void delete(Long id, Handler<AsyncResult<JsonObject>> asyncResultHandler) {
        geometryRepository.deleteById(id, handler -> {
            if (handler.succeeded()) {
                JsonObject json = new JsonObject();
                json.put(SUCCESS_STATUS_FIELD, true);

                asyncResultHandler.handle(Future.succeededFuture(json));
            } else {
                JsonObject json = new JsonObject();
                json.put(SUCCESS_STATUS_FIELD, false);
                json.put(ERROR_MESSAGE_FIELD, handler.cause().getMessage());
                asyncResultHandler.handle(Future.succeededFuture(json));
            }
        });
    }
}
