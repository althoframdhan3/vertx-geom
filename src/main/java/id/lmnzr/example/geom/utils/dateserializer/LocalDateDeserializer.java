package id.lmnzr.example.geom.utils.dateserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import id.lmnzr.example.geom.utils.DateUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;


/**
 * The type Local date deserializer.
 */
public class LocalDateDeserializer extends StdDeserializer<LocalDate> {
    /**
     * Instantiates a new Date deserializer.
     */
    public LocalDateDeserializer() {
        this(null);
    }

    /**
     * Instantiates a new Date deserializer.
     *
     * @param c the class
     */
    public LocalDateDeserializer(Class<?> c) {
        super(c);
    }

    /**
     * Override deserialize process to convert iso date time string to GMT+7
     * date time string
     *
     * @param jsonParser             the json parser
     * @param deserializationContext the deserialization context
     * @return local date time string
     * @throws IOException the input output exception
     */
    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String dateString = jsonParser.getText().trim();

        LocalDate localDate;

        try {
            localDate = DateUtils.fromIsoString(dateString).toLocalDate();
        } catch (DateTimeParseException ex) {
            try {
                localDate = DateUtils.fromMySqlString(dateString).toLocalDate();
            } catch (DateTimeParseException ignored) {
                localDate = deserializationContext.parseDate(dateString).toInstant().atZone(DateUtils.getDefaultZoneId()).toLocalDate();
            }
        }

        return localDate;
    }
}
