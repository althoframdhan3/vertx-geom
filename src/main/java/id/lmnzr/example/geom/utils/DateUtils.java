package id.lmnzr.example.geom.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Date time related utilities
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DateUtils {
    /**
     * MYSQL date time format
     */
    public static final String FORMAT_MYSQL = "yyyy-MM-dd HH:mm:ss";

    /**
     * Year-month-date format
     */
    public static final String FORMAT_YMD = "yyyy-MM-dd";

    /**
     * Gets default zone id.
     *
     * @return the default zone id
     */
    public static ZoneId getDefaultZoneId() {
        ZoneId zoneId = ZoneId.systemDefault();

        if (System.getenv("TZ") != null && !System.getenv("TZ").isEmpty() && !System.getenv("TZ").isEmpty()) {
            String timeZoneString = System.getenv("TZ");
            zoneId = ZoneId.of(timeZoneString);
        }

        return zoneId;
    }

    /**
     * Local date time to format string.
     *
     * @param localDateTime the local date time
     * @param format        the format
     * @return the string
     */
    public static String format(LocalDateTime localDateTime, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);

        return localDateTime.format(formatter);
    }

    /**
     * Format my sql string.
     *
     * @param localDateTime the local date time
     * @return the string
     */
    public static String formatMySql(LocalDateTime localDateTime) {
        return format(localDateTime, FORMAT_MYSQL);
    }

    /**
     * From iso string local date time.
     *
     * @param isoDateTime the iso date time
     * @return the local date time
     */
    public static LocalDateTime fromIsoString(String isoDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(isoDateTime, formatter);

        return LocalDateTime.ofInstant(zonedDateTime.toInstant(), getDefaultZoneId());
    }

    /**
     * From my sql string local date time.
     *
     * @param mysqlString the mysql string
     * @return the local date time
     */
    public static LocalDateTime fromMySqlString(String mysqlString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_MYSQL);
        return LocalDateTime.parse(mysqlString, formatter);
    }

    /**
     * Now local date time.
     *
     * @return the local date time
     */
    public static LocalDateTime now() {
        return LocalDateTime.now(getDefaultZoneId());
    }
}

