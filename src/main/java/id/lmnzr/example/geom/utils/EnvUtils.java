package id.lmnzr.example.geom.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.URI;

/**
 * The type Env utils.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EnvUtils {
    /**
     * Gets port from env.
     *
     * @return the port from env
     */
    public static int getPortFromEnv() {
        String portEnv = System.getenv("PORT");
        return (portEnv != null) ? Integer.parseInt(portEnv) : 8585;
    }

    /**
     * Gets database uri from env.
     *
     * @return the database uri from env
     */
    public static URI getDatabaseURIfromEnv() {
        String dbUrl = System.getenv("GEOM_DB");
        return URI.create((dbUrl != null) ? dbUrl : "mysql://root:root@localhost:3306/test");
    }
}
