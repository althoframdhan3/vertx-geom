package id.lmnzr.example.geom.utils.dateserializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import id.lmnzr.example.geom.utils.DateUtils;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * The type Local date time serializer.
 */
public class LocalDateTimeSerializer extends StdSerializer<LocalDateTime> {
    /**
     * Instantiates a new Date serializer.
     */
    public LocalDateTimeSerializer() {
        this(null);
    }

    /**
     * Instantiates a new Date serializer.
     *
     * @param c the class
     */
    public LocalDateTimeSerializer(Class<LocalDateTime> c) {
        super(c);
    }

    /**
     * Override serialize process to convert iso date object to GMT+7 date time
     * string
     *
     * @param localDateTime      the date
     * @param jsonGenerator      the json generator
     * @param serializerProvider the serializer provider
     * @throws IOException the input output exception
     */
    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(DateUtils.format(localDateTime, DateUtils.FORMAT_MYSQL));
    }
}
