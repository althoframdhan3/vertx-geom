package id.lmnzr.example.geom.database.driver;

import io.vertx.core.Vertx;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import java.net.URI;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(VertxUnitRunner.class)
@PowerMockIgnore("javax.management.*")
public class ClientFactoryTest {
    private Vertx vertx;

    @Before
    public void init(){
        vertx = Vertx.vertx();
    }

    @Test
    public void createShared(TestContext ctx) {
        ClientFactory.createShared(vertx, URI.create("mysql://root@localhost:3306/db"), ClientFactory.Driver.MARIADB);
        ctx.assertTrue(true);
    }

    @Test
    public void createNonShared(TestContext ctx) {
        ClientFactory.createNonShared(vertx, URI.create("mysql://root@localhost:3306/db"), ClientFactory.Driver.MARIADB);
        ctx.assertTrue(true);
    }

    //TODO: Remove if Driver Implemented
    @Test
    public void createShared_unknownDriver(TestContext ctx) {
        JDBCClient jdbcClient = ClientFactory.createShared(vertx, URI.create("mysql://root@localhost:3306/db"), ClientFactory.Driver.MYSQL);
        ctx.assertNull(jdbcClient);
    }

    //TODO: Remove if Driver Implemented
    @Test
    public void createNonShared_unknownDriver(TestContext ctx) {
        JDBCClient jdbcClient = ClientFactory.createNonShared(vertx, URI.create("mysql://root@localhost:3306/db"), ClientFactory.Driver.MYSQL);
        ctx.assertNull(jdbcClient);
    }
}