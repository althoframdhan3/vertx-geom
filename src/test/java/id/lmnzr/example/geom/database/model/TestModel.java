package id.lmnzr.example.geom.database.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "valueOf")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestModel {
    @JsonProperty(value = EntityCrudRepository.ID)
    private Long id;

    @JsonProperty(value = EntityCrudRepository.DELETED)
    private Boolean deleted;

    @JsonProperty(value = EntityCrudRepository.CREATED_TIME)
    private LocalDateTime createdTime;

    @JsonProperty(value = EntityCrudRepository.LAST_MODIFIED_TIME)
    private LocalDateTime lastModifiedTime;

    @JsonProperty(value = EntityCrudRepository.DELETED_TIME)
    private LocalDateTime deletedTime;

    public static TestModel valueOf(Long id){
        return TestModel.builder()
            .id(id)
            .deleted(false)
            .build();
    }

}
