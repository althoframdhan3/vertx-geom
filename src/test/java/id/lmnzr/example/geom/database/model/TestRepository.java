package id.lmnzr.example.geom.database.model;

import io.vertx.ext.jdbc.JDBCClient;

public class TestRepository extends EntityCrudRepository{
    /**
     * Instantiates a new Master model.
     *
     * @param jdbcClient the jdbc client
     */
    public TestRepository(JDBCClient jdbcClient) {
        super(jdbcClient);
        this.tableName = "test";
    }
}
