package id.lmnzr.example.geom.utils.dateserializer;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.Month;

public class LocalDateTimeSerializerTest {

    @SneakyThrows
    @Test
    public void serialize() {
        Writer jsonWriter = new StringWriter();
        JsonGenerator jsonGenerator = new JsonFactory().createGenerator(jsonWriter);
        SerializerProvider serializerProvider = new ObjectMapper().getSerializerProvider();
        new LocalDateTimeSerializer().serialize(LocalDateTime.of(2000, Month.JANUARY,1,11,22,33), jsonGenerator, serializerProvider);
        jsonGenerator.flush();
        Assert.assertEquals("\"2000-01-01 11:22:33\"", jsonWriter.toString());
    }
}